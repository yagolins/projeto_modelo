import { EquipeDetalhesComponent } from './equipe-detalhes/equipe-detalhes.component';
import { CreateEquipeComponent } from './criar-equipe/criar-equipe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EquipeListComponent } from './lista-equipes/lista-equipes.component';

const routes: Routes = [
  { path: '', redirectTo: 'equipe', pathMatch: 'full' },
  { path: 'equipes', component: EquipeListComponent },
  { path: 'add', component: CreateEquipeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }