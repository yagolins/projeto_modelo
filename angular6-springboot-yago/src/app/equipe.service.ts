import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  private baseUrl = 'modelo/equipes';

  constructor(private http: HttpClient) { }

  getEquipe(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  criarEquipe(equipe: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, equipe);
  }

  alterarEquipe(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteEquipe(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getAllEquipes(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
