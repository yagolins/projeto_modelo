import { EquipeService } from './../equipe.service';
import { Equipe } from './../equipe';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-criar-equipe',
  templateUrl: './criar-equipe.component.html',
  styleUrls: ['./criar-equipe.component.scss']
})
export class CreateEquipeComponent implements OnInit {

  equipe: Equipe = new Equipe();
  submitted = false;

  constructor(private equipeService: EquipeService) { }

  ngOnInit() {
  }

  newEquipe(): void {
    this.submitted = false;
    this.equipe = new Equipe();
  }

  save() {
    this.equipeService.criarEquipe(this.equipe)
      .subscribe(data => console.log(data), error => console.log(error));
    this.equipe = new Equipe();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }
}
