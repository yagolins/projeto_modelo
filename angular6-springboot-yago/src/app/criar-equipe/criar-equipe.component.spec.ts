import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEquipeComponent } from './criar-equipe.component';

describe('CriarEquipeComponent', () => {
  let component: CreateEquipeComponent;
  let fixture: ComponentFixture<CreateEquipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEquipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
