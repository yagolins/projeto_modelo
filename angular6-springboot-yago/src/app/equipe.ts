export class Equipe {
    id: number;
    nome: string;
    anoFundacao: number;
    qtdeAtletas: number;
    active: boolean;
}