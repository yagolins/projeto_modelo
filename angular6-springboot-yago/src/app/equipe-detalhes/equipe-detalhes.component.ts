import { Equipe } from './../equipe';
import { Component, OnInit, Input } from '@angular/core';
import { EquipeService } from '../equipe.service';
import { EquipeListComponent } from '../lista-equipes/lista-equipes.component';

@Component({
  selector: 'app-equipe-detalhes',
  templateUrl: './equipe-detalhes.component.html',
  styleUrls: ['./equipe-detalhes.component.scss']
})
export class EquipeDetalhesComponent implements OnInit {

  @Input() equipe: Equipe;

  constructor(private equipeService: EquipeService, private listComponent: EquipeListComponent) { }

  ngOnInit() {
  }
}
