import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateEquipeComponent } from './criar-equipe/criar-equipe.component';
import { EquipeDetalhesComponent } from './equipe-detalhes/equipe-detalhes.component';
import { EquipeListComponent } from './lista-equipes/lista-equipes.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    CreateEquipeComponent,
    EquipeDetalhesComponent,
    EquipeListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
