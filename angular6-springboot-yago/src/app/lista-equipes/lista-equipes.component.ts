import { Observable } from "rxjs";
import { EquipeService } from "./../equipe.service";
import { Equipe } from "./../equipe";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-",
  templateUrl: "./lista-equipes.component.html",
  styleUrls: ["./lista-equipes.component.scss"]
})
export class EquipeListComponent implements OnInit {
  equipes: Observable<Equipe[]>;

  constructor(private equipeService: EquipeService) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.equipes = this.equipeService.getAllEquipes();
  }

  deleteEquipe(id: number) {
    this.equipeService.deleteEquipe(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}