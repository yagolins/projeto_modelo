package com.crudbasico.crudyago.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crudbasico.crudyago.entidade.Equipe;

@Repository
public interface EquipeRepositorio extends JpaRepository<Equipe, Long>{

}
