package com.crudbasico.crudyago.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crudbasico.crudyago.entidade.Equipe;
import com.crudbasico.crudyago.repositorio.EquipeRepositorio;

@RestController
@RequestMapping("/modelo")
public class EquipeController {

	@Autowired
    private EquipeRepositorio equipeRepositorio;
	
	@GetMapping("/equipes")
    public List<Equipe> getAllEquipes() {
        return equipeRepositorio.findAll();
    }

	@GetMapping("/equipes/{id}")
    public ResponseEntity<Equipe> getEquipeById(@PathVariable(value = "id") Long equipeId)
        throws Exception {
        Equipe equipe = equipeRepositorio.findById(equipeId)
          .orElseThrow(() -> new Exception("Equipe não encontrada com este id => " + equipeId));
        return ResponseEntity.ok().body(equipe);
    }
    
	@PostMapping("/equipes")
    public Equipe criarEquipe(@Valid @RequestBody Equipe equipe) {
        return equipeRepositorio.save(equipe);
    }

	@PutMapping("/equipes/{id}")
    public ResponseEntity<Equipe> alterarEquipe(@PathVariable(value = "id") Long equipeId,
         @Valid @RequestBody Equipe equipeAlterar) throws Exception {
		Equipe equipe = equipeRepositorio.findById(equipeId)
        .orElseThrow(() -> new Exception("Equipe não encontrada com este id => " + equipeId));

		equipe.setNome(equipeAlterar.getNome());
		equipe.setAnoFundacao(equipeAlterar.getAnoFundacao());
		equipe.setQtdeAtletas(equipeAlterar.getQtdeAtletas());
        final Equipe equipeAlterada = equipeRepositorio.save(equipe);
        return ResponseEntity.ok(equipeAlterada);
    }

	@DeleteMapping("/equipes/{id}")
    public Map<String, Boolean> deleteEquipe(@PathVariable(value = "id") Long equipeId)
         throws Exception {
        Equipe equipe = equipeRepositorio.findById(equipeId)
       .orElseThrow(() -> new Exception("Equipe não encontrada com este id => " + equipeId));

        equipeRepositorio.delete(equipe);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

