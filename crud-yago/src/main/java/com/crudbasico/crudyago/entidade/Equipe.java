package com.crudbasico.crudyago.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "equipes")
public class Equipe {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "nome_equipe", nullable = false)
	private String nome;
	@Column(name = "qtde-atletas", nullable = false)
    private Integer qtdeAtletas;
	@Column(name = "ano-fundacao", nullable = false)
    private Integer anoFundacao;
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getQtdeAtletas() {
		return qtdeAtletas;
	}
	public void setQtdeAtletas(Integer qtdeAtletas) {
		this.qtdeAtletas = qtdeAtletas;
	}
	public Integer getAnoFundacao() {
		return anoFundacao;
	}
	public void setAnoFundacao(Integer anoFundacao) {
		this.anoFundacao = anoFundacao;
	}
	
	@Override
    public String toString() {
        return "Equipe [id=" + id + ", nome=" + nome + ", Ano de fundação=" + anoFundacao + ", Quantidade de atletas=" + qtdeAtletas
       + "]";
    }
}
