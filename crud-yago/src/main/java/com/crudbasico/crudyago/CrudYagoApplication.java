package com.crudbasico.crudyago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudYagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudYagoApplication.class, args);
	}

}
