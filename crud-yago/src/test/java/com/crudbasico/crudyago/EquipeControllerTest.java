package com.crudbasico.crudyago;

/*import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.crudbasico.crudyago.entidade.Equipe;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EquipeControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testGetAllEquipes() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/equipes",
				HttpMethod.GET, entity, String.class);
		
		assertNotNull(response.getBody());
	}

	@Test
	public void testGetEquipeById() {
		Equipe equipe = restTemplate.getForObject(getRootUrl() + "/equipes/1", Equipe.class);
		System.out.println(equipe.getNome());
		assertNotNull(equipe);
	}

	@Test
	public void testCreateEquipe() {
		Equipe equipe = new Equipe();
		equipe.setNome("Botafogo");
		equipe.setAnoFundacao(1992);
		equipe.setQtdeAtletas(32);

		ResponseEntity<Equipe> postResponse = restTemplate.postForEntity(getRootUrl() + "/equipes", equipe, Equipe.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdateEquipe() {
		int id = 1;
		Equipe equipe = restTemplate.getForObject(getRootUrl() + "/equipes/" + id, Equipe.class);
		equipe.setNome("equipe1");
		equipe.setNome("equipe2");

		restTemplate.put(getRootUrl() + "/equipes/" + id, equipe);

		Equipe updatedEquipe = restTemplate.getForObject(getRootUrl() + "/equipes/" + id, Equipe.class);
		assertNotNull(updatedEquipe);
	}

	@Test
	public void testDeleteEquipe() {
		int id = 2;
		Equipe equipe = restTemplate.getForObject(getRootUrl() + "/equipes/" + id, Equipe.class);
		assertNotNull(equipe);

		restTemplate.delete(getRootUrl() + "/equipes/" + id);

		try {
			equipe = restTemplate.getForObject(getRootUrl() + "/equipes/" + id, Equipe.class);
		} catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}*/
